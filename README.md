# php-mtg/mtg-api-com-mtgjson-object

A library that implements the php-mtg/mtg-api-com-mtgjson-interface library.

![coverage](https://gitlab.com/php-mtg/mtg-api-com-mtgjson-object/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-mtg/mtg-api-com-mtgjson-object/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-mtg/mtg-api-com-mtgjson-object ^8`


## Basic Usage

This library may be used the following way :

```php

use PhpMtg\Mtgjson\MtgjsonComApiEndpoint;

/* @var $client \Psr\Http\Client\ClientInterface */

$endpoint = new MtgjsonComApiEndpoint($client);

$collection = $this->_endpoint->getSetList();

foreach($collection->getSets() as $setResume)
{
	/** @var \PhpMtg\Mtgjson\MtgjsonComApiSetMetaInterface $meta */
	$meta = $this->_endpoint->getSet($setResume->getCode());
	
	$set = $meta->getSet();
	// do something to set
}

```


## License

MIT (See [license file](LICENSE)).
