<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgjson-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgjson;

use PhpExtended\DataProvider\JsonStringDataProvider;
use PhpExtended\HttpMessage\RequestFactory;
use PhpExtended\HttpMessage\StreamFactory;
use PhpExtended\HttpMessage\UriFactory;
use PhpExtended\Reifier\Reifier;
use PhpExtended\Reifier\ReifierInterface;
use PhpExtended\Uuid\UuidInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Http\Message\UriFactoryInterface;
use RuntimeException;
use Throwable;

/**
 * ApiComMtgjsonEndpoint class file.
 * 
 * This is a simple implementation of the ApiComMtgjsonEndpointInterface.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class ApiComMtgjsonEndpoint implements ApiComMtgjsonEndpointInterface
{
	
	public const HOST = 'https://mtgjson.com/api/v5/';
	
	/**
	 * The http client.
	 *
	 * @var ClientInterface
	 */
	protected ClientInterface $_httpClient;
	
	/**
	 * The uri factory.
	 *
	 * @var UriFactoryInterface
	 */
	protected UriFactoryInterface $_uriFactory;
	
	/**
	 * The request factory.
	 *
	 * @var RequestFactoryInterface
	 */
	protected RequestFactoryInterface $_requestFactory;
	
	/**
	 * The stream factory.
	 *
	 * @var StreamFactoryInterface
	 */
	protected StreamFactoryInterface $_streamFactory;
	
	/**
	 * The reifier.
	 *
	 * @var ReifierInterface
	 */
	protected ReifierInterface $_reifier;
	
	/**
	 * Builds a new endpoint with its dependancies.
	 *
	 * @param ClientInterface $client
	 * @param ?UriFactoryInterface $uriFactory
	 * @param ?RequestFactoryInterface $requestFactory
	 * @param ?StreamFactoryInterface $streamFactory
	 * @param ?ReifierInterface $reifier
	 */
	public function __construct(
		ClientInterface $client,
		?UriFactoryInterface $uriFactory = null,
		?RequestFactoryInterface $requestFactory = null,
		?StreamFactoryInterface $streamFactory = null,
		?ReifierInterface $reifier = null
	) {
		$this->_httpClient = $client;
		$this->_uriFactory = $uriFactory ?? new UriFactory();
		$this->_requestFactory = $requestFactory ?? new RequestFactory();
		$this->_streamFactory = $streamFactory ?? new StreamFactory();
		$this->_reifier = $reifier ?? new Reifier();
		
		$configuration = $this->_reifier->getConfiguration();
		
		$configuration->setIterableInnerType(ApiComMtgjsonBooster::class, 'contents', 'int');
		$configuration->setIterableInnerType(ApiComMtgjsonBoosterPolicy::class, 'boosters', ApiComMtgjsonBooster::class);
		$configuration->setIterableInnerType(ApiComMtgjsonBoosterPolicy::class, 'sheets', ApiComMtgjsonBoosterSheet::class);
		$configuration->setIterableInnerType(ApiComMtgjsonBoosterSheet::class, 'cards', 'string');
		$configuration->setIterableInnerType(ApiComMtgjsonCard::class, 'artistIds', UuidInterface::class);
		$configuration->setIterableInnerType(ApiComMtgjsonCard::class, 'attractionLights', 'int');
		$configuration->setIterableInnerTypes(ApiComMtgjsonCard::class, ['availability', 'boosterTypes', 'cardParts', 'colorIdentity', 'colorIndicator', 'colors', 'finishes', 'frameEffects', 'keywords', 'names', 'printings', 'promoTypes', 'reverseRelated', 'subsets', 'subtypes', 'supertypes', 'types'], 'string');
		$configuration->setIterableInnerType(ApiComMtgjsonCard::class, 'foreignData', ApiComMtgjsonForeignData::class);
		$configuration->setIterableInnerTypes(ApiComMtgjsonCard::class, ['otherFaceIds', 'originalPrintings', 'rebalancedPrintings', 'variations'], UuidInterface::class);
		$configuration->setIterableInnerType(ApiComMtgjsonCard::class, 'rulings', ApiComMtgjsonRuling::class);
		$configuration->setIterableInnerTypes(ApiComMtgjsonCardPrice::class, ['mtgoPrices', 'mtgoFoilPrices', 'paperPrices', 'paperFoilPrices'], ApiComMtgjsonPrice::class);
		$configuration->setIterableInnerTypes(ApiComMtgjsonCardType::class, ['supertypes', 'subtypes'], 'string');
		$configuration->setIterableInnerType(ApiComMtgjsonCardTypeCollection::class, 'types', ApiComMtgjsonCardType::class);
		$configuration->setIterableInnerTypes(ApiComMtgjsonDeck::class, ['mainBoard', 'sideBoard', 'commander'], ApiComMtgjsonCard::class);
		$configuration->setIterableInnerType(ApiComMtgjsonDeckCollection::class, 'data', ApiComMtgjsonDeckResume::class);
		$configuration->setIterableInnerTypes(ApiComMtgjsonRelatedCard::class, ['reverseRelated', 'spellbook'], 'string');
		$configuration->setIterableInnerType(ApiComMtgjsonSealedProductContent::class, 'card', ApiComMtgjsonSealedProductContentCard::class);
		$configuration->setIterableInnerType(ApiComMtgjsonSealedProductContent::class, 'deck', ApiComMtgjsonSealedProductContentDeck::class);
		$configuration->setIterableInnerType(ApiComMtgjsonSealedProductContent::class, 'other', ApiComMtgjsonSealedProductContentOther::class);
		$configuration->setIterableInnerType(ApiComMtgjsonSealedProductContent::class, 'pack', ApiComMtgjsonSealedProductContentPack::class);
		$configuration->setIterableInnerType(ApiComMtgjsonSealedProductContent::class, 'sealed', ApiComMtgjsonSealedProductContentSealed::class);
		$configuration->setIterableInnerType(ApiComMtgjsonSealedProductContent::class, 'variable', ApiComMtgjsonSealedProductContentVariable::class);
		$configuration->setIterableInnerType(ApiComMtgjsonSealedProductContentConfig::class, 'card', ApiComMtgjsonSealedProductContentCard::class);
		$configuration->setIterableInnerType(ApiComMtgjsonSealedProductContentConfig::class, 'deck', ApiComMtgjsonSealedProductContentDeck::class);
		$configuration->setIterableInnerType(ApiComMtgjsonSealedProductContentConfig::class, 'sealed', ApiComMtgjsonSealedProductContentSealed::class);
		$configuration->setIterableInnerType(ApiComMtgjsonSealedProductContentDeckCollection::class, 'deck', ApiComMtgjsonSealedProductContentDeck::class);
		$configuration->setIterableInnerType(ApiComMtgjsonSealedProductContentVariable::class, 'deck', ApiComMtgjsonSealedProductContentDeck::class);
		$configuration->setIterableInnerType(ApiComMtgjsonSealedProductContentVariable::class, 'configs', ApiComMtgjsonSealedProductContentConfig::class);
		$configuration->setIterableInnerType(ApiComMtgjsonSet::class, 'booster', ApiComMtgjsonBoosterPolicy::class);
		$configuration->setIterableInnerType(ApiComMtgjsonSet::class, 'cards', ApiComMtgjsonCard::class);
		$configuration->setIterableInnerType(ApiComMtgjsonSet::class, 'decks', ApiComMtgjsonSetDeck::class);
		$configuration->setIterableInnerType(ApiComMtgjsonSet::class, 'languages', 'string');
		$configuration->setIterableInnerType(ApiComMtgjsonSet::class, 'sealedProduct', ApiComMtgjsonSealedProduct::class);
		$configuration->setIterableInnerType(ApiComMtgjsonSet::class, 'tokens', ApiComMtgjsonCard::class);
		$configuration->setIterableInnerType(ApiComMtgjsonSet::class, 'translations', ApiComMtgjsonSetTranslation::class);
		$configuration->setIterableInnerType(ApiComMtgjsonSetCollection::class, 'data', ApiComMtgjsonSetResume::class);
		$configuration->setIterableInnerType(ApiComMtgjsonSetDeck::class, 'cards', ApiComMtgjsonSetDeckCardQuantity::class);
		$configuration->setIterableInnerType(ApiComMtgjsonSetDeck::class, 'sealedProductUuids', UuidInterface::class);
		$configuration->setIterableInnerType(ApiComMtgjsonSetResume::class, 'sealedProduct', ApiComMtgjsonSealedProduct::class);
		$configuration->setIterableInnerType(ApiComMtgjsonSetResume::class, 'languages', 'string');
		$configuration->setIterableInnerType(ApiComMtgjsonSetResume::class, 'decks', ApiComMtgjsonSetDeck::class);
		$configuration->setIterableInnerType(ApiComMtgjsonSourceProduct::class, 'etched', UuidInterface::class);
		$configuration->setIterableInnerType(ApiComMtgjsonSourceProduct::class, 'foil', UuidInterface::class);
		$configuration->setIterableInnerType(ApiComMtgjsonSourceProduct::class, 'nonfoil', UuidInterface::class);
		
		$configuration->addFieldNameAlias(ApiComMtgjsonCard::class, 'manaValue', 'convertedManaCost');
		$configuration->addFieldNameAlias(ApiComMtgjsonCard::class, 'faceManaValue', 'faceConvertedManaCost');
		$configuration->addFieldNameAlias(ApiComMtgjsonSetTranslation::class, 'ancientGreek', 'ancient greek');
		$configuration->addFieldNameAlias(ApiComMtgjsonSetTranslation::class, 'chineseSimplified', 'chinese simplified');
		$configuration->addFieldNameAlias(ApiComMtgjsonSetTranslation::class, 'chineseTraditional', 'chinese traditional');
		$configuration->addFieldNameAlias(ApiComMtgjsonSetTranslation::class, 'portugueseBrazil', 'portuguese (brazil)');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\ApiComMtgjson\ApiComMtgjsonEndpointInterface::getDeckLists()
	 * @throws RuntimeException should not happen
	 */
	public function getDeckList() : ApiComMtgjsonDeckCollection
	{
		$url = self::HOST.'DeckList.json';
		
		try
		{
			$uri = $this->_uriFactory->createUri($url);
			$request = $this->_requestFactory->createRequest('GET', $uri);
			$response = $this->_httpClient->sendRequest($request);
			$json = new JsonStringDataProvider($response->getBody()->__toString());
			
			return $this->_reifier->reify(ApiComMtgjsonDeckCollection::class, $json->provideOne());
		}
		// @codeCoverageIgnoreStart
		catch(Throwable $exc)
		{
			$message = 'Failed to get data from url {url}';
			$context = ['{url}' => $url];
			
			throw new RuntimeException(\strtr($message, $context), -1, $exc);
		}
		// @codeCoverageIgnoreEnd
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\ApiComMtgjson\ApiComMtgjsonEndpointInterface::getDeck()
	 * @throws RuntimeException should not happen
	 */
	public function getDeck(string $deckFilePath) : ApiComMtgjsonDeckMeta
	{
		$url = self::HOST.'decks/'.$deckFilePath.'.json';
		
		try
		{
			$uri = $this->_uriFactory->createUri($url);
			$request = $this->_requestFactory->createRequest('GET', $uri);
			$response = $this->_httpClient->sendRequest($request);
			$json = new JsonStringDataProvider($response->getBody()->__toString());
			
			return $this->_reifier->reify(ApiComMtgjsonDeckMeta::class, $json->provideOne());
		}
		// @codeCoverageIgnoreStart
		catch(Throwable $exc)
		{
			$message = 'Failed to get data from url {url}';
			$context = ['{url}' => $url];
			
			throw new RuntimeException(\strtr($message, $context), -1, $exc);
		}
		// @codeCoverageIgnoreEnd
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\ApiComMtgjson\ApiComMtgjsonEndpointInterface::getSetList()
	 * @throws RuntimeException should not happen
	 */
	public function getSetList() : ApiComMtgjsonSetCollection
	{
		$url = self::HOST.'SetList.json';
		
		try
		{
			$uri = $this->_uriFactory->createUri($url);
			$request = $this->_requestFactory->createRequest('GET', $uri);
			$response = $this->_httpClient->sendRequest($request);
			$json = new JsonStringDataProvider($response->getBody()->__toString());
			
			return $this->_reifier->reify(ApiComMtgjsonSetCollection::class, $json->provideOne());
		}
		// @codeCoverageIgnoreStart
		catch(Throwable $exc)
		{
			$message = 'Failed to get data from url {url}';
			$context = ['{url}' => $url];
			
			throw new RuntimeException(\strtr($message, $context), -1, $exc);
		}
		// @codeCoverageIgnoreEnd
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\ApiComMtgjson\ApiComMtgjsonEndpointInterface::getSet()
	 * @throws RuntimeException should not happen
	 */
	public function getSet(string $setCode) : ApiComMtgjsonSetMeta
	{
		$url = self::HOST.$setCode.'.json';
		
		try
		{
			$uri = $this->_uriFactory->createUri($url);
			$request = $this->_requestFactory->createRequest('GET', $uri);
			$response = $this->_httpClient->sendRequest($request);
			$json = new JsonStringDataProvider($response->getBody()->__toString());
			
			return $this->_reifier->reify(ApiComMtgjsonSetMeta::class, $json->provideOne());
		}
		// @codeCoverageIgnoreStart
		catch(Throwable $exc)
		{
			$message = 'Failed to get data from url {url}';
			$context = ['{url}' => $url];
			
			throw new RuntimeException(\strtr($message, $context), -1, $exc);
		}
		// @codeCoverageIgnoreEnd
	}
	
}
