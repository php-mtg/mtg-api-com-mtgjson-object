<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgjson-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\HttpMessage\Response;
use PhpExtended\HttpMessage\StringStream;
use PhpExtended\Reifier\ReificationException;
use PhpMtg\ApiComMtgjson\ApiComMtgjsonEndpoint;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * ApiComMtgjsonEndpointTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpMtg\ApiComMtgjson\ApiComMtgjsonEndpoint
 *
 * @internal
 *
 * @small
 */
class ApiComMtgjsonEndpointTest extends TestCase
{
	
	/**
	 * The endpoint to test.
	 * 
	 * @var ApiComMtgjsonEndpoint
	 */
	protected ApiComMtgjsonEndpoint $_endpoint;
	
	/**
	 * public function testSetList() : void
	 * {
	 * $collection = $this->_endpoint->getSetList();
	 * $collectionVersion = $collection->meta->version;
	 * $this->assertEquals(5, $collectionVersion->getMajor());
	 * $this->assertEquals(2, $collectionVersion->getMinor());
	 * $this->assertEquals(1, $collectionVersion->getPatch());.
	 *
	 * foreach($collection->data as $setResume)
	 * {
	 * $this->assertNotEmpty($setResume->code);
	 * }
	 * }
	 *
	 * public function testDeckList() : void
	 * {
	 * $collection = $this->_endpoint->getDeckList();
	 * $collectionVersion = $collection->meta->version;
	 * $this->assertEquals(5, $collectionVersion->getMajor());
	 * $this->assertEquals(2, $collectionVersion->getMinor());
	 * $this->assertEquals(1, $collectionVersion->getPatch());
	 *
	 * foreach($collection->data as $deckResume)
	 * {
	 * $this->assertNotEmpty($deckResume->fileName);
	 * }
	 * }
	 *
	 * public function testDeckContents() : void
	 * {
	 * $now = new DateTimeImmutable();
	 * $twoYearsAgo = $now->add(DateInterval::createFromDateString('-2 years'));
	 * $collection = $this->_endpoint->getDeckList();
	 *
	 * foreach($collection->data as $deckResume)
	 * {
	 * // only check the last two years
	 * if(null !== $deckResume->releaseDate && $deckResume->releaseDate->getTimestamp() < $twoYearsAgo->getTimestamp())
	 * {
	 * continue;
	 * }
	 *
	 * try
	 * {
	 * $meta = $this->_endpoint->getDeck($deckResume->fileName);
	 * $deck = $meta->data;
	 * $this->assertNotEmpty($deck->code);
	 * }
	 * catch(ReificationException $exc)
	 * {
	 * throw new RuntimeException('Failed to check set : '.$deckResume->code, -1, $exc);
	 * }
	 * }
	 * }
	 *
	 * public function testSetContents() : void
	 * {
	 * $now = new DateTimeImmutable();
	 * $twoYearsAgo = $now->add(DateInterval::createFromDateString('-2 years'));
	 * $collection = $this->_endpoint->getSetList();
	 *
	 * foreach($collection->data as $setResume)
	 * {
	 * // only check the last two years
	 * if(null !== $setResume->releaseDate && $setResume->releaseDate->getTimestamp() < $twoYearsAgo->getTimestamp())
	 * {
	 * continue;
	 * }
	 *
	 * try
	 * {
	 * $meta = $this->_endpoint->getSet($setResume->code);
	 * $set = $meta->data;
	 * $this->assertNotEmpty($set->code);
	 * foreach($set->booster as $boosterPolicy)
	 * {
	 * foreach($boosterPolicy->sheets as $boosterSheet)
	 * {
	 * foreach($boosterSheet->cards as $uuid => $weight)
	 * {
	 * $message = 'Card from set {set}, booster policy {pname}, uuid {uuid} is not a string';
	 * $context = ['{set}' => $set->code, '{pname}' => $boosterPolicy->name ?? '(empty)', '{uuid}' => (string) $uuid];
	 * $this->assertIsString($uuid, \strtr($message, $context));
	 *
	 * $message = 'Card from set {set}, booster policy {pname}, uuid {uuid} has not a weight as integer : {val}';
	 * $context = ['{set}' => $set->code, '{pname}' => $boosterPolicy->name ?? '(empty)', '{uuid}' => (string) $uuid, '{val}' => \json_encode($weight)];
	 * $this->assertIsNumeric($weight, \strtr($message, $context));
	 * }
	 * }
	 * }
	 * }
	 * catch(ReificationException $exc)
	 * {
	 * throw new RuntimeException('Failed to check set : '.$setResume->code, -1, $exc);
	 * }
	 * }
	 * }
	 *
	 * /**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$client = new class() implements ClientInterface
		{
			public function sendRequest(RequestInterface $request) : ResponseInterface
			{
				\sleep(1);
				$options = ['http' => ['user_agent' => 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:92.0) Gecko/20100101 Firefox/92.0']];
				$data = @\file_get_contents($request->getUri()->__toString(), false, \stream_context_create($options));
				if(false === $data)
				{
					$data = \file_get_contents($request->getUri()->__toString(), false, \stream_context_create($options));
				}
				$body = new StringStream($data);
				
				return (new Response())->withBody($body);
			}
		};
		
		$this->_endpoint = new ApiComMtgjsonEndpoint($client);
	}
	
}
